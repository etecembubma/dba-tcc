<?php

class Database{

    private static $db;

    private function __construct($db_nome){

        $db_host = "localhost";
        $db_usuario = "root";
        $db_senha = "";
        $db_driver = "mysql";

        try {

            $opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_PERSISTENT => true);

            self::$db = new PDO("$db_driver:host=" . $db_host . "; dbname=" . $db_nome . "; charset=utf8;", $db_usuario, $db_senha, $opcoes);

            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            
            die("Connection Error: " . $e->getMessage());
        }
    }

    public static function conectar($db_nome){

        if (!self::$db) {
            new Database($db_nome);
        }

        return self::$db;
    }
}