<?php 

    require_once('CRUD.php');

    class CrudCadUser implements CRUD{

        public function insert($usuario){
            $sql = "INSERT INTO usuarios() VALUES(default, ? ,? ,? ,? ,?)";
            $pdo = Database::conectar();
            $stm = $pdo->prepare($sql);
            $stm->bindParam(1, $usuario->cpf_cnpj);
            $stm->bindParam(2, $usuario->email);
            $stm->bindParam(3, $usuario->login);
            $stm->bindParam(4, $usuario->senha);
            $stm->bindParam(5, $usuario->fk_controle_pacote);
            if($stm->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function update($usuario){
            $sql = "UPDATE usuarios SET cpf_cnpj = ?, email = ?, login = ?, senha = ?, fk_controle_pacote = ? WHERE id = ?";
            $pdo = Database::conectar();
            $stm = $pdo->prepare($sql);
            $stm->bindParam(1, $usuario->cpf_cnpj);
            $stm->bindParam(2, $usuario->email);
            $stm->bindParam(3, $usuario->login);
            $stm->bindParam(4, $usuario->senha);
            $stm->bindParam(5, $usuario->fk_controle_pacote);
            $stm->bindParam(6, $usuario->id);
            if($stm->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function selectAll(){
            $sql = "SELECT * FROM usuarios";
            $pdo = Database::conectar();
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $result = $stm->fetchAll(PDO::FETCH_OBJ);
            return $result;
        }

        public function selectWhere($col, $val){
            $sql "SELECT * FROM usuarios WHERE ? = ?";
            $pdo = Database::conectar();
            $stm = $pdo->prepare($sql);
            $stm->bindParam(1, $col);
            $stm->bindParam(2, $val);
            $stm->execute();
            $result = $stm->fetch(PDO::FETCH_OBJ);
            return $result;
        }
    }