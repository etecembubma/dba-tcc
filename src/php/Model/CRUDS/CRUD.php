<?php
    interface CRUD{

        public function insert($var);

        public function update($var);

        public function selectAll();

        public function selectWhere($col, $var);

    }