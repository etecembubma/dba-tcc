<?php 

    require_once('CRUDS/crud_cad_user.php')

    class ModCadUser extends CrudCadUser{
        
        public function createDatabase($database){
            $sql = 'CREATE DATABASE ? charset="utf8"';
            $pdo = Database::conectar();
            $stm = $pdo->prepare($sql);
            if($stm->execute()){
                return true;
            }else{
                return false;
            }
        }
    }