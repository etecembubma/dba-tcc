<?php

class Usuario{
    
        private id;
        private login;
        private senha;
        private nivelAcesso;
        private ativo;
        private dataCadastro;
        private foreingKey;
        private to;

        public getId(){
            return $this->id;
        }

        public setId($id){
            $this->id = $id;
        }

        public getlogin(){
            return $this->login;
        }

        public setlogin($login){
            $this->login = $login;
        }

        public getsenha(){
            return $this->senha;
        }

        public setsenha($senha){
            $this->senha = $senha;
        }

        public getnivelAcesso(){
            return $this->nivelAcesso;
        }

        public setnivelAcesso($nivelAcesso){
            $this->nivelAcesso = $nivelAcesso;
        }

        public getativo(){
            return $this->ativo;
        }

        public setativo($ativo){
            $this->ativo = $ativo;
        }

        public getdataCadastro(){
            return $this->dataCadastro;
        }

        public setdataCadastro($dataCadastro){
            $this->dataCadastro = $dataCadastro;
        }

        public getForeignKey(){
            return $this->foreignKey;
        }

        public setForeignKey($foreignKey){
            $this->foreignKey = $foreignKey;
        }

        public getTo(){
            return $this->to;
        }

        public setTo($to){
            $this->to = $to;
        }

}