<?php 

    class Funcionario{

        private id_funcionario;
        private nome_funcionario;
        private cpf_funcionario;
        private datanasc_funcionario;
        private tipo_funcionario;
        private situacao_funcionario;
        private cargo_funcionario;
        private carteiratrab_funcionario;
        private pis_funcionario;
        private usuario;
        private endFuncionario;
        private contatoFuncionario;
        private dadosBancFuncionario;

        public getId_funcionario(){
            return $this->id_funcionario;
        }

        public setId_funcionario($id){
            $this->id_funcionario = $id;
        }

        public getNome_funcionario(){
            return $this->nome_funcionario;
        }

        public setNome_funcionario($nome){
            $this->nome_funcionario = $nome;
        }

        public getCpf_funcionario(){
            return $this->cpf_funcionario;
        }

        public setCpf_funcionario($cpf){
            $this->cpf_funcionario = $cpf;
        }

        public getDatanasc_funcionario(){
            return $this->datanasc_funcionario;
        }

        public setDatanasc_funcionario($nasc){
            $this->datanasc_funcionario = $nasc;
        }

        public getTipo_funcionario(){
            return $this->tipo_funcionario;
        }

        public setTipo_funcionario($tipo){
            $this->tipo_funcionario = $tipo;
        }

        public getSituacao_funcionario(){
            return $this->situacao_funcionario;
        }

        public setSituacao_funcionario($situacao){
            $this->situacao_funcionario = $situacao;
        }

        public getCargo_funcionario(){
            return $this->cargo_funcionario;
        }

        public setCargo_funcionario($cargo){
            $this->cargo_funcionario = $cargo;
        }

        public getCarteiratrab_funcionario(){
            return $this->carteiratrab_funcionario;
        }

        public setCarteiratrab_funcionario($carteira){
            $this->carteiratrab_funcionario = $carteira;
        }
        
        public getPis_funcionario(){
            return $this->pis_funcionario;
        }

        public setPis_funcionario($pis){
            $this->pis_funcionario = $pis;
        }

        public getUsuario(){
            return $this->usuario;
        }

        public setUsuario($usuario){
            $this->usuario = $usuario;
        }

        public getEndFuncionario(){
            return $this->endFuncionario;
        }

        public setEndFuncionario($endFuncionario){
            $this->endFuncionario = $endFuncionario;
        }

        public getContatoFuncionario(){
            return $this->contatoFuncionario;
        }

        public setContatoFuncionario($contatoFuncionario){
            $this->contatoFuncionario = $contatoFuncionario;
        }

        public getDadosBancFuncionario(){
            return $this->dadosBancFuncionario;
        }

        public setDadosBancFuncionario($dadosBancFuncionario){
            $this->dadosBancFuncionario = $dadosBancFucionario;
        }

    }