<?php 

class Endereco{

    private id;
    private logradouro;
    private cep;
    private cidade;
    private estado;
    private foreingKey;
    private to;

    public getId(){
        return $this->id;
    }

    public setId($id){
        $this->id = $id;
    }

    public getLogradouro(){
        return $this->logradouro;
    }

    public setLogradouro($logradouro){
        $this->logradouro = $logradouro;
    }

    public getCep(){
        return $this->cep;
    }

    public setCep($cep){
        $this->cep = $cep;
    }

    public getCidade(){
        return $this->cidade;
    }

    public setCidade($cidade){
        $this->cidade = $cidade;
    }

    public getEstado(){
        return $this->estado;
    }

    public setEstado($estado){
        $this->estado = $estado;
    }

    public getForeingKey(){
        return $this->foreingKey;
    }

    public setForeingKey($foreingKey){
        $this->foreingKey = $foreingKey;
    }

    public getTo(){
        return $this->to;
    }

    public setTo($to){
        $this->to = $to;
    }

}