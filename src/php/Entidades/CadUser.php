<?php 
    require_once('Helpers/Verify.php');

    class CadUSer {

        private verify = new Verify();

        private id;
        private cpf_cnpj;
        private email;
        private login;
        private senha;
        private fk_controle_pacote

        public function getId(){
            return $this->id;
        }

        public function setId($valor){
            $thid->id = $valor;
        }

        public function getCpf_cnpj(){
            return $this->cpf_cnpj;
        }

        public function setCpf_cnpj($valor){
            if(strlen($valor) == 11 && $verify->isCPFValido($valor)){
                $this->cpf_cnpj = $valor;
                return true;
            }else if(srtlen($valor) == 14 && $verify->isCNPJValido($valor)){
                $this->cpf_cnpj = $valor;
                return true;
            }else{
                return false;
            }
        }

        public function getEmail(){
            return $this->email;
        }

        public function setEmail($valor){
            if($verify->validarEmail($valor)){
                $this->email = $valor; 
                return true;
            }else{
                return false;
            }
        }

        public function getLogin(){
            return $this->login;
        }

        public function setLogin($valor){
            $this->login = $valor;
        }

        public function getFk_controle_pacote(){
            return $this->fk_controle_pacote;
        }

        public function setFk_controle_pacote($valor){
            $this->fk_controle_pacote = $valor;
        }

    }