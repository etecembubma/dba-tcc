<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>
<body>
    <div class="layout-container-flex">

        <div class="layout-sub-container-flex layout-index-container-menu">

            <h1 class="layout-index-title-one font-index-title-one">Vector<span>[<span>X</span>]</span></h1>

            <nav class="layout-index-menu">
                
                <ul class="style-index-menu">
                    <li>
                        <a href="" class="font-common" >Home</a>
                    </li>
                    <li>
                        <a href="" class="font-common" >Sobre</a>
                    </li>
                    <li>
                        <a href="" class="font-common" >Duvidas</a>
                    </li>
                    <li>
                        <a href="" class="font-common" >Login</a>
                    </li>
                </ul>

            </nav>

        </div>

        <div class="layout-sub-container-flex layout-index-container-titles">

            <h2 class="font-index-subTitle">DBA - Administre seu negocio com facilidade</h2>
            <h2 class="font-index-subTitle">Rapido, Facil e Em qualquer lugar</h2>

        </div>

        <div class="layout-sub-container-flex layout-index-container-callButton_img">
            <a class="style-btn-CTA">Cadastre-se já</a>
            <div>
                <img src="img/img1/img-resposividade-index.png" alt="">
            </div>
        </div>

    </div>
    <div class="layout-container-flex style-container2-index">
        <div class="layout-sub-container-flex layout-sub-container2">
                <h3 class="font-h3-index">
                    Melhor nao sei</br>
                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid consectetur adipisicing elit. Aliquid</span>
                </h3>
                <h3 class="font-h3-index">
                    Maior nao sei</br>
                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid consectetur adipisicing elit. Aliquid</span>
                </h3>
                <h3 class="font-h3-index">
                    Movel nao sei</br>
                    <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid consectetur adipisicing elit. Aliquid</span>
                </h3>
        </div>
    </div>

    <div class="layout-container-flex style-container3-index">
    <p class="font-index-subTitle">Algum texto Aqui</p>
        <div class="layout-sub-container-flex">
            <div class="utilitario-flex-row">
                
                <div class="layout-cardSobre-index utilitario-bgGray">
                    <img src="img/img1/rafael.jpg" alt="">
                    <p>Rafael Passianoto minha rola</p>
                    <p>"Sou um loiro por isso essa empresa vai dar certo"</p>
                    <p>Admistrador do Banco De Dados</p>
                    <p>Email: nao tenho fucking ideia</p>
                </div>

                <div class="layout-cardSobre-index utilitario-bgGray">
                    <img src="img/img1/rafael.jpg" alt="">
                    <p>Rafael Passianoto minha rola</p>
                    <p>"Sou um loiro por isso essa empresa vai dar certo"</p>
                    <p>Admistrador do Banco De Dados</p>
                    <p>Email: nao tenho fucking ideia</p>
                </div>

                <div class="layout-cardSobre-index utilitario-bgGray">
                    <img src="img/img1/rafael.jpg" alt="">
                    <p>Rafael Passianoto minha rola</p>
                    <p>"Sou um loiro por isso essa empresa vai dar certo"</p>
                    <p>Admistrador do Banco De Dados</p>
                    <p>Email: nao tenho fucking ideia</p>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>