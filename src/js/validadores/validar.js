function mostraErro(elemPai,span,texto){
    document.getElementById(span).textContent = texto;
    elemPai.focus();
    elemPai.addEventListener('change',function(){
        document.getElementById(span).textContent = "";
    });
}

function validaForm(frm) {

    var regNume =  /[0-9]/;
    
    var regEspeChar = /[`€ƒ†‡‰ŠŒŽ•™œš¢£¤¥¦§©¬«®¯°±µ¶»½¿Æ÷¾¼~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;

    var p = document.querySelector('#ErroCadastro');

    if(frm.nome.value == "" || frm.nome.value == null || frm.nome.value.length < 3 
        || regNume.test(frm.nome.value) == true || frm.nome.value.indexOf(" ") > -1 
        || regEspeChar.test(frm.nome.value) == true) {

        mostraErro( frm.nome, "erroNome", 'Por favor, insira o nome');
        return false;

    }else if(frm.sobrenome.value == "" || frm.sobrenome.value == null || frm.sobrenome.value.length < 3 
              || regNume.test(frm.sobrenome.value) == true || frm.sobrenome.value.indexOf(" ") > -1 
              || regEspeChar.test(frm.sobrenome.value) == true){

        mostraErro( frm.sobrenome, 'erroSobrenome', 'Por favor, insira o sobrenome');
        return false;

    }else if(frm.nomeUsuario.value == "" || frm.nomeUsuario.value == null || frm.nomeUsuario.value.length < 3
            || frm.nomeUsuario.value.indexOf(" ") > -1 ){

        mostraErro( frm.nomeUsuario, 'erroUsuario', 'Por favor, insira o nome de usuario');
        return false;

    }else if(frm.email.value.indexOf("@") == -1 || frm.email.value.indexOf(".") == -1 
    || frm.email.value == "" || frm.email.value == null){

        mostraErro( frm.email, 'erroEmail', 'Pro favor, insira o email');
        return false;

    }else if(frm.dataNasci.value == "" || frm.dataNasci.value == null ){

        mostraErro( frm.dataNasci, 'erroNascimento', 'Por favor, insira a data de nascimento');
        return false;

    } else if(frm.senha.value == "" || frm.senha.value == null){

        mostraErro( frm.senha, 'erroSenha', 'Por favor, insira a senha');
        return false;

    }else if(frm.senha.value.length < 6){

        mostraErro( frm.senha, 'erroSenha', 'e necessario 6 digitos no minino');
        return false;

    } else if(frm.confSenha.value == "" || frm.confSenha.value == null){

       mostraErro( frm.confSenha, 'erroConfSenha', 'confirme a senha');
       return false;

    } else if(frm.confSenha.value != frm.senha.value){

       mostraErro( frm.confSenha, 'erroConfSenha', 'as senhas estao diferentes');
       return false;

    }else{
        return true;
    }

}




