var gulp = require('gulp'); 

var sass = require('gulp-sass');

var uglify = require('gulp-uglify');

var cssNano = require('gulp-cssnano');

var imageMin = require('gulp-imagemin');

gulp.task('sass', function(){
    return gulp.src('src/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('src/css'));
});

gulp.task('minifyJs',function(){
    return gulp.src('src/js/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('cssNano',function(){
    return gulp.src('src/css/**/*.css')
    .pipe(cssNano())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('imageMin',function(){
    return gulp.src('src/img/**/*.+(png|jpg|gif|svg)')
    .pipe(imageMin())
    .pipe(gulp.dest('dist/img'));
});

gulp.task('font',function(){
    return gulp.src('src/fonts/*')
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('php',function(){
    return gulp.src('src/php/**/*.php')
    .pipe(gulp.dest('dist/php'));
});

gulp.task('mainFile',function(){
    return gulp.src('src/*.+(php|html)')
    .pipe(gulp.dest('dist/'));
});

gulp.task('watch',['sass'],function(){
    gulp.watch('src/scss/**/*.scss',['sass']);
});

gulp.task('build',['minifyJs','cssNano','imageMin','font','php','mainFile'], function(){
    console.log('construindo aplicaçao');
});